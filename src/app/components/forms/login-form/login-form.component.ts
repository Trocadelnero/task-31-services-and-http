import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth/auth.service";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  user = { username: '', password: '' };
  loginMessage: string;
  loginError: string;

  constructor(private auth: AuthService, private router: Router) { }
  ngOnInit(): void { }

  async onLoginClicked() {
    this.loginError = '';
    this.loginMessage = '';
    try {
      const result: any = await this.auth.login(this.user);
      console.log(result);
      if (result.status < 400) {
        this.loginMessage = `You are logged in!
      redirect will be featured in the near future`;
      // this.router.navigateByUrl('/dashboard'); 
      }
    } catch (e) {
      console.error(e.error);
      return this.loginError = e.error.error
    } finally {

    }
  }
}




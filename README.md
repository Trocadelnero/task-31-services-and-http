
# Angular Service and HttpClient -Task 31

## Description
**Implementing an Authservice and adding http-request for register/login components.**
- LoginForm.component
- RegisterForm.component
* based on previous project [Task30]https://gitlab.com/Trocadelnero/task-30-angular-components
- auth.Service
* register(user): Promise<any>
  Sends an HTTP request using the HttpClient to register a user.

* login(user): Promise<any>
Sends an HTTP request using the HttpClient to attempt a login using provided credentials.
```
Components (Login, Register)
You must inject the service into the register and login components you created in the previous task. 
The login component should execute the login method from the AuthService when the user clicks on the login button
The register component should execute the register method from the AuthService when the user clicks on the register button.

You can check the response of the method in the Network tab of the Browsers' Developer Tools. 
```



**This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.**

## Development server

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Aknowledgements
- Api provided by our Great Teacher Dewald Els

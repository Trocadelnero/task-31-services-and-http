import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth/auth.service";

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  
  user = { username: '', password: '' }
  
  registerError: string;
  registerMessage: string;
  
  /**Register-TOGGLES
   * Confirm password to reveal checkbox.
   * Check box toggles Register-btn.
   * Register button goes onRegisterClicked()
   */
  confirm = { password: '' }
  showRegister: boolean = false;
  isLoading: boolean = false;
  toggleRegister() { this.showRegister = !this.showRegister, this.registerError = '', this.registerMessage = ''; };
  //Later: toggle-service?
  constructor(private auth: AuthService, private router: Router) { }


  ngOnInit(): void {
    this.confirm.password = null;
  }
  /**
   * Try to register.
   * @param user
   */
  async onRegisterClicked() {
   
    try {
      this.toggleRegister()
      this.isLoading = true;
      const result: any = await this.auth.register(this.user);
      console.log(result);
      if (result.status < 400) {
        this.registerMessage = 'You are registered, remember your credentials!';
      }
    } catch (e) {
      console.error(e.error);
   this.registerError = e.error.error
    }
    try{

    } finally {
      this.isLoading = false;
     
    }

  }

}


